## Run the final image
```
docker run -it --rm --env-file ./env -p 8443:8443 --cpus 2.0 --user 65534:65534 --read-only --name nginx-cdn nginx-cdn:dev
```