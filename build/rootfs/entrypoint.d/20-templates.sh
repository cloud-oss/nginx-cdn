#!/bin/sh

set -e

BASENAME=$( basename "$0" )

if [ -z ${DNS_RESOLVERS+x} ]; then
   DNS_RESOLVERS="$(grep nameserver /etc/resolv.conf | cut -d' ' -f2 | xargs)"
   export DNS_RESOLVERS
   echo "[$(date -R)] [$BASENAME] DNS Resolvers: ${DNS_RESOLVERS}"
fi

TEMPLATE_DIR="/etc/nginx/templates"
SUFFIX=".template"
OUTPUT_DIR="/tmp/nginx"

# shellcheck disable=SC2016,SC2046
DEFINED_VARS=$(printf '${%s} ' $(env | cut -d= -f1))

mkdir -p "$OUTPUT_DIR"

if [ ! -w "$OUTPUT_DIR" ]; then
    echo "[$(date -R)] [$BASENAME] ERROR: $OUTPUT_DIR is not writable"
    return 0
fi

find "$TEMPLATE_DIR" -follow -type f -name "*$SUFFIX" -print | while read -r TEMPLATE; do
    RELATIVE_PATH="${TEMPLATE#$TEMPLATE_DIR/}"
    OUTPUT_PATH="$OUTPUT_DIR/${RELATIVE_PATH%$SUFFIX}"
    SUBDIR=$(dirname "$RELATIVE_PATH")
    # create a subdirectory where the template file exists
    mkdir -p "$OUTPUT_DIR/$SUBDIR"
    echo "[$(date -R)] [$BASENAME] Create $OUTPUT_PATH from $TEMPLATE"
    envsubst "$DEFINED_VARS" < "$TEMPLATE" > "$OUTPUT_PATH"
  done
