#!/bin/sh

set -e

BASENAME=$( basename "$0" )

if [ "${NGINX_PROXY_CACHE_CLEAR}" = "true" ]; then
  rm -rf /opt/nginx/proxy_cache/*
  echo "[$(date -R)] [$BASENAME] Proxy Cache cleared"
fi
