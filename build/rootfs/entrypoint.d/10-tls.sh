#!/bin/sh
set -e

BASENAME=$( basename "$0" )

if [ "$AUTO_TLS" = "false" ]; then
   echo "[$(date -R)] [$BASENAME] Bring you own SSL Certificate active"
else
   echo "[$(date -R)] [$BASENAME] Create selfsigned certificate"
   mkdir -p /tmp/tls
   RANDFILE="/tmp/tls/tls.rnd"
   export RANDFILE

   SSL_SAN="DNS:localhost,DNS:$(hostname -s),IP:$(ip route get 1 | awk '{print $NF;exit}')"
   export SSL_SAN

   openssl genpkey -genparam -algorithm EC -pkeyopt ec_paramgen_curve:P-256 -out /tmp/tls/tls.param
   openssl req -x509 -nodes -days 3650 \
               -newkey ec:/tmp/tls/tls.param \
               -subj "/O=Acme Co/CN=Microservice Fake Certificate" \
               -addext "subjectAltName=${SSL_SAN}" \
               -keyout /tmp/tls/tls.key \
               -out /tmp/tls/tls.pem \
               > /dev/null 2>&1
fi
