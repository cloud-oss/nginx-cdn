#!/bin/sh
set -e

BASENAME=$( basename "$0" )

if [ -z ${S3_ACCESS_KEY_ID+x} ]; then
   echo "[$(date -R)] [$BASENAME] Access Key ID: missing"
   exit 1
else
   echo "[$(date -R)] [$BASENAME] Access Key ID: $S3_ACCESS_KEY_ID"
fi

if [ -z ${S3_SECRET_KEY+x} ]; then
   echo "[$(date -R)] [$BASENAME] S3_SECRET_KEY: missing"
   exit 1
else
   echo "[$(date -R)] [$BASENAME] S3_SECRET_KEY: $S3_SECRET_KEY"
fi

if [ -z ${S3_BUCKET_NAME+x} ]; then
   echo "[$(date -R)] [$BASENAME] S3_BUCKET_NAME: missing"
   exit 1
else
   echo "[$(date -R)] [$BASENAME] S3_BUCKET_NAME: $S3_BUCKET_NAME"
fi

if [ -z ${S3_SERVER+x} ]; then
   echo "[$(date -R)] [$BASENAME] S3_SERVER: missing"
   exit 1
else
   echo "[$(date -R)] [$BASENAME] S3_SERVER: $S3_SERVER"
fi

if [ -z ${S3_SERVER_PORT+x} ]; then
   echo "[$(date -R)] [$BASENAME] S3_SERVER_PORT: missing"
   exit 1
else
   echo "[$(date -R)] [$BASENAME] S3_SERVER_PORT: $S3_SERVER_PORT"
fi

if [ -z ${S3_SERVER_PROTO+x} ]; then
   echo "[$(date -R)] [$BASENAME] S3_SERVER_PROTO: missing"
   exit 1
else
   echo "[$(date -R)] [$BASENAME] S3_SERVER_PROTO: $S3_SERVER_PROTO"
   if [ "${S3_SERVER_PROTO}" != "http" ] && [ "${S3_SERVER_PROTO}" != "https" ]; then
      echo "[$(date -R)] [$BASENAME] S3_SERVER_PROTO: invalid value (expected: http, https)"
      exit 1
   fi
fi

if [ -z ${S3_REGION+x} ]; then
   echo "[$(date -R)] [$BASENAME] S3_REGION: missing"
   exit 1
else
   echo "[$(date -R)] [$BASENAME] S3_REGION: $S3_REGION"
fi

if [ -z ${S3_STYLE+x} ]; then
   echo "[$(date -R)] [$BASENAME] S3_STYLE: missing"
   exit 1
else
   echo "[$(date -R)] [$BASENAME] S3_STYLE: $S3_STYLE"
   if [ "${S3_STYLE}" != "path" ] && [ "${S3_STYLE}" != "virtual" ]; then
      echo "[$(date -R)] [$BASENAME] S3_STYLE: invalid value (expected: path, virtual)"
      exit 1
   fi
fi

if [ -z ${AWS_SIGS_VERSION+x} ]; then
   echo "[$(date -R)] [$BASENAME] AWS_SIGS_VERSION: missing"
   exit 1
else
   echo "[$(date -R)] [$BASENAME] AWS_SIGS_VERSION: $AWS_SIGS_VERSION"
   if [ "${AWS_SIGS_VERSION}" != "2" ] && [ "${AWS_SIGS_VERSION}" != "4" ]; then
      echo "[$(date -R)] [$BASENAME] AWS_SIGS_VERSION: invalid value (expected: 2, 4)"
      exit 1
   fi
fi
