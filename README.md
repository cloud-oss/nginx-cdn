# Nginx S3 Cache Server
Source: [nginx.org](http://nginx.org)

Alpine based Image for running Nginx Proxy Cache for S3 as container.

Based on NGINX, Inc. S3 Caching Gateway (https://github.com/nginxinc/nginx-s3-gateway)

## Desired scope
- base image for static web content located inside S3 bucket
- cache s3 content for fast access
- immutable

## Features
- automatically creates selfsigned SSL certificate (ecdsa) on startup
- bring your own ssl certificate (expects: /tmp/tls/tls.pem,/tmp/tls/tls.key)
- documentroot located at /var/www
- default listens on port 8443 supporting https and http2
- ssl hardened
- access logs using proxy headers (x-forwarded-for)
- runs under any user-id
- publish server-status at :8000/
- publish healthcheck at :8000/healthz
- timezone support for logs (env TZ=Europe/Berlin)
- modules included: brotli, njs
- configuration files splitted
- supports read-only deployment
- cache path within volume at /tmp

## Usage
- can be used as final image
- uses env vars for dynamic runtime properties

## ENV vars

| VAR                        | Default     | expect nginx property      |
| ---------------------------|-------------| ---------------------------|
| AUTO_TLS                   | true        | gen ssl certificate        |
| NGINX_WORKER_PROCESSES     | 1           | worker_processes           |
| NGINX_WORKER_CONNECTIONS   | 1024        | worker_connections         |
| NGINX_RLIMIT_NOFILE        | 2048        | worker_rlimit_nofile       |
| NGINX_ERROR_LOG            | info        | error_log level            |
| NGINX_SENDFILE_MAX_CHUNK   | 64k         | sendfile_max_chunk         |
| NGINX_KEEPALIVE_TIMEOUT    | 65s         | keepalive_timeout          |
| NGINX_OFC_MAX              | 1000        | open_file_cache max        |
| NGINX_OFC_INACTIVE         | 20s         | open_file_cache inactive   |
| NGINX_OFC_VALID            | 30s         | open_file_cache_valid      |
| NGINX_PROXY_CACHE_SIZE     | 256m        | proxy_cache_path max_size  |
| NGINX_PROXY_CACHE_INACTIVE | 15m         | proxy_cache_path inactive  |
| NGINX_PROXY_CACHE_CLEAR    | false       | clear proxy cache on start |
| S3_BUCKET_NAME             |             | s3: name of the bucket     |
| S3_ACCESS_KEY_ID           |             | s3: access key             |
| S3_SECRET_KEY              |             | s3: secret key             |
| S3_SERVER                  |             | s3: endpoint fqdn          |
| S3_SERVER_PORT             |             | s3: port                   |
| S3_SERVER_PROTO            |             | s3: protocol (http, https) |
| S3_REGION                  |             | s3: region                 |
| S3_STYLE                   |             | s3: bucket access (virtual,path) |
| S3_DEBUG                   |             | s3: enable debug logs      |
| AWS_SIGS_VERSION           |             | s3: signing model (2,4)    |
